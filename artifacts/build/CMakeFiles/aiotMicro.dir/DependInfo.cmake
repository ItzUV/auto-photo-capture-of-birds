# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/builds/ItzUV/auto-photo-capture-of-birds/src/exutils.cpp" "/builds/ItzUV/auto-photo-capture-of-birds/build/CMakeFiles/aiotMicro.dir/src/exutils.cpp.o"
  "/builds/ItzUV/auto-photo-capture-of-birds/src/main.cpp" "/builds/ItzUV/auto-photo-capture-of-birds/build/CMakeFiles/aiotMicro.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../"
  "../include"
  "/usr/include/opencv"
  "../src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.8

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /builds/ItzUV/auto-photo-capture-of-birds

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /builds/ItzUV/auto-photo-capture-of-birds/build

# Utility rule file for codestyle.

# Include the progress variables for this target.
include CMakeFiles/codestyle.dir/progress.make

CMakeFiles/codestyle: codecheck.txt


codecheck.txt:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/builds/ItzUV/auto-photo-capture-of-birds/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Checking for MISRA Code compliance"
	cd /builds/ItzUV/auto-photo-capture-of-birds && /usr/bin/cppcheck --dump --quiet /builds/ItzUV/auto-photo-capture-of-birds/src/main.cpp /builds/ItzUV/auto-photo-capture-of-birds/src/exutils.cpp
	cd /builds/ItzUV/auto-photo-capture-of-birds && python3 /usr/share/cppcheck/addons/misra.py --rule-texts=/usr/share/cppcheck/MISRA_C_2012.txt || exit 0
	cd /builds/ItzUV/auto-photo-capture-of-birds && rm -rf

codestyle: CMakeFiles/codestyle
codestyle: codecheck.txt
codestyle: CMakeFiles/codestyle.dir/build.make

.PHONY : codestyle

# Rule to build all files generated by this target.
CMakeFiles/codestyle.dir/build: codestyle

.PHONY : CMakeFiles/codestyle.dir/build

CMakeFiles/codestyle.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/codestyle.dir/cmake_clean.cmake
.PHONY : CMakeFiles/codestyle.dir/clean

CMakeFiles/codestyle.dir/depend:
	cd /builds/ItzUV/auto-photo-capture-of-birds/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /builds/ItzUV/auto-photo-capture-of-birds /builds/ItzUV/auto-photo-capture-of-birds /builds/ItzUV/auto-photo-capture-of-birds/build /builds/ItzUV/auto-photo-capture-of-birds/build /builds/ItzUV/auto-photo-capture-of-birds/build/CMakeFiles/codestyle.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/codestyle.dir/depend


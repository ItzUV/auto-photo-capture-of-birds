# Components Required

1. Web cam/Pi cam - For frame capturing
2. Birds - For Detection
3. Raspberry pi - For Image processing
4. Whatsapp - For Sending bird Images 
5. SD Card
6. USB Cable

# Required Skills

1. Pip
2. Python
3. Open CV
4. Raspberry pi
5. Tensor Flow

# Flow Diagram

![Flow_chart](Images/flow_chart.png)

Created on [Draw.io](https://draw.io)


# Psuedo Code

```console
Capture_video = cv2.VideoCapture(0)
```
```console
if Capture_video.isOpened():  # then try to read the first frame  
______value, frame = Capture_video.read()  
else:  
______value = False
```
```console
While Value is true:
Tensorflow_process_image()
```
```console
Detect_bird ()
```  
```console
while detect_bird is True:
snap()
```
```console
if snap is true
send_snap()
call Capture_video()
```

# Project Plan

## Project Goal

To create a project that cab capture automatic photos of birds with the help of an hidden camera and sends the captured frame to our whatsapp.  

## Output 
Get the picture of bird(s) appearing in the frame of camera to your own whatsapp.

## Reference

1. [How to Set Up TensorFlow Object Detection on the Raspberry Pi](https://www.youtube.com/watch?v=npZ-8Nj1YwY) - [GitHub Repo](https://github.com/EdjeElectronics/TensorFlow-Object-Detection-on-the-Raspberry-Pi)
2. [How To Run TensorFlow Lite on Raspberry Pi for Object Detection](https://www.youtube.com/watch?v=aimSGOAUI8Y)
3. [Bird Watching Camera with intel RealSense, Python, TensorFlow and OpenCV](https://www.youtube.com/watch?v=s7FjOD1fBcQ&t=489s)
4. [Raspberry pi Bird Detector](https://medium.com/@NickVasilyevv/raspberry-pi-bird-detector-d3469eb16f90#:~:text=The%20idea%20was%20simple%2C%20I,post%20the%20video%20to%20twitter.)
5. [NVIDIA Jetson Powered Deep Learning Bird Camera](https://github.com/burningion/deep-learning-bird-camera)
6. [Automatically Send Message with Image Via Whatsapp](https://www.youtube.com/watch?v=TtdDnY2GQQo)
7. [TensorFlow Lite Python object detection example with Pi Camera](https://github.com/tensorflow/examples/tree/master/lite/examples/object_detection/raspberry_pi)

## Project Sections

|SrNo| Section          | Goal                              |
|:--:|  :----           |  :----                            |
| 1  | The Analyist     | Frame Capturing & Bird Detection  |
| 2  | The Photographer | Camera trigger on bird detection  |
| 3  | The Communicator | Sending required frame to whatsapp|

## Project Sequence

|Sr No| Sequence                            |          
|:--: |  :----                              |
|1    |Frame Capturing & Bird Detection     |
|2    |Camera trigger on bird detection     |
|3    |Sending required frame to whatsapp   |           

## Section Details

 1. The Analyist     : Frame capturing & Bird detection

    1. Initialising camera
    2. Capturing train of frames (aka Video / Live Stream)
    3. Detecting birds in the captured frame  

 2. The Photographer : Camera Trigger on bird detection
    
    1. Checking Each frame to find bird
    2. If bird detected trigger camera
    3. Save the bird detected frame on the local machine   

 3. The Communicator : Sending Required frame to whatsapp

    1. Pick the saved image from local machine
    2. Establish communication channel with Pi & Whatsapp
    3. Push the image onto whatsapp  
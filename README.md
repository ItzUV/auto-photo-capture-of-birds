# Auto-Photo-Capture-Of-Brids `(A-PCB)`

## Introduction

This project aims to capture automatic photos of birds with the help of an hidden camera and sends the captured frame to our whatsapp.  

![bird](Images/bird.jpg)

## Documentation 
For developers see detailed Documentation on the components of the project in the [Wiki]()

## Project Overview 

1.  [Project Plan Excel](https://docs.google.com/spreadsheets/d/1OW_e1c6VPQ39bae9Sl9kYx3grmbIpJPH31hwwB3TJ-g/edit?usp=sharing)


## Contributing
Help us improve the project.  
Refere [Contributing Guidelines](https://gitlab.com/ItzUV/auto-photo-capture-of-birds/-/blob/master/Contributing.md) before proceeding

Ways you can help:

1.  Choose from the existing issue and work on those issues.
2.  Feel like the project could use a new feature, make an issue to discuss how it can be implemented and work on it.
3.  Find a bug create an Issue and report it.
4.  Review Issues or Merge Requests, give the developers the feedback.
5.  Fix Documentation.

## Contributors 

#### Team Lead 
- Yuvraj Kadale - @itzUV

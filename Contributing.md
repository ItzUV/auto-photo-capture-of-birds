# Contributing Guidelines

I want to make it as simple as possible for our users and contributors to reach us and communicate with one another, so that the project can continue to improve. 

## Development 

This document provides a collection of instructions that will assist you with the contribution process.
I gratefully accept all contributions from anyone who wishes to improve/add new functionality to this project or resolve bugs.

Below you will find the process and workflow for contributing to `Auto-Photo-Capture-Of-Birds`.

## Step 1 : Find an issue
- Take a look at the Existing Issues or create your **own** Issues!
- Ask for the Issue to be assigned to you after which you can start working on it.
- Note : Every change in this project should/must have an associated issue. 

## Step 2 : Fork the Project
- Fork this Repository. This will create a Local Copy of this Repository on your Github Profile. Keep a reference to the original project in `upstream` remote.
.
- Development happens in the [develop branch](https://github.com/OpenRoberta/openroberta-lab/tree/develop). Please send PRs against that branch.
```
$ git clone https://gitlab.com/<Your Username>/auto-photo-capture-of-birds.git 
$ cd auto-photo-capture-of-birds
$ git remote add upstream https://gitlab.com/ItzUV/auto-photo-capture-of-birds.git
```

- If you have already forked the project, update your copy before working.
```
$ git remote update
$ git checkout <branch-name>
$ git rebase upstream/<branch-name>
```
## Step 3 : Branch
Create a new branch. Use its name to identify the issue you are addressing.
```
# It will create a new branch with name Branch_Name and switch to that branch

$ git checkout -b branch_name
```
## Step 4 : Work on the issue assigned
- Work on the issue(s) assigned to you. 
- Add all the files/folders needed.
- After you've made changes or made your contribution to the project add changes to the branch you've just created by:
```
# To add all new files to branch Branch_Name

$ git add .
```
## Step 5 : Commit
- To commit give a descriptive message for convenience 

```
# This message get associated with all files you have changed

$ git commit -m "message"
```
## Step 6 : Work Remotely
- Now you are ready to your work to the remote repository.
- When your work is ready and complies with the project conventions, upload your changes to your fork:

```
# To push your work to your remote repository

$ git push -u origin Branch_Name
```

## Step 7 : Pull Request
- Go to your repository in browser and click on compare and pull requests. Then add a title and description to your pull request that explains your contribution.

- Voila! Your Pull Request has been submitted and will be reviewed by the moderators/admins/project manger.

## Need more help?
You can refer to the following articles on basics of Git and Github and can also @Itzuv or @SejalGupta, in case you are stuck:
- [Forking a Repo](https://help.github.com/en/github/getting-started-with-github/fork-a-repo)
- [Cloning a Repo](https://help.github.com/en/desktop/contributing-to-projects/creating-an-issue-or-pull-request)
- [How to create a Pull Request](https://opensource.com/article/19/7/create-pull-request-github)
- [Getting started with Git and GitHub](https://towardsdatascience.com/getting-started-with-git-and-github-6fcd0f2d4ac6)
- [Learn GitHub from Scratch](https://lab.github.com/githubtraining/introduction-to-github)

